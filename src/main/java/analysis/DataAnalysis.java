package analysis;

import structure.PersonTree;

import java.io.IOException;

public class DataAnalysis {

        public static void main(String[] args) throws IOException {

        new Keyword(args);
        ExcelUtil excel = new ExcelUtil();
        PersonList.createPersonList();
        PersonTree pTree = new PersonTree(PersonList.listPerson);
        excel.createNewSortedFile(pTree.getGroupCollection());

    }

}
