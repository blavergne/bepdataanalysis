package analysis;

import java.util.ArrayList;
import java.util.List;

public class Person {

    //Class variable memebers
    public List<String> cellList = new ArrayList<String>();
    int[] categoryArray = new int[Keyword.keywords.size()];
    int columnBound = ExcelUtil.getColumnBound();
    int matchesCount = 0;

    //Constructor
    Person(int rowIndex){

        addCellData(rowIndex);//Add cell information to person cellList

        for(int colIndex = 3; colIndex < columnBound; colIndex++) {
            if(ExcelUtil.getRowCellData(rowIndex, colIndex) != null) {
                for(int i = 0; i < Keyword.keywords.size(); i++) {
                    int keywordLen = Keyword.keywords.get(i).length();
                    if (ExcelUtil.getRowCellData(rowIndex, colIndex).toString().contains(Keyword.keywords.get(i).substring(0, keywordLen-1))) {
                        recordKeywordFrequency(Keyword.keywords.get(i));
                    }
                }
            }
            else
                continue;

        }

        numberOfKeywordMatches(categoryArray);

    }

    //Getters
    public int[] getCategoryArray(){
        return categoryArray;
    }

    public List<String> getCellList(){
        return cellList;
    }

    public int getMatchesCount(){
        return matchesCount;
    }

    /*
    *****************************************************Member Methods********************************************
     */

    public void numberOfKeywordMatches(int[] array){
        for(int i = 0; i < array.length; i++){
            if(array[i] == 1){
                matchesCount++;
            }
        }
    }

    public void addCellData(int row){

        for(int cell = 0; cell < columnBound; cell++){
            cellList.add(ExcelUtil.getRowCellData(row, cell));
            //System.out.print(ExcelUtil.getRowCellData(row, cell));
        }
    }

    public void recordKeywordFrequency(String keyword){
        categoryArray[Keyword.keywords.indexOf(keyword)] = 1;
    }

}


