package analysis;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import java.util.List;
import java.io.*;

public class ExcelUtil {

    //Excel file
    private static String FILE_NAME = "CLAUDIA-ANALYSIS MATRIX.xlsx";
    private static XSSFSheet sheet;
    private static XSSFWorkbook workbook;
    private static FileInputStream excelFile;
    private static int columnBound = 0;
    private static int rowBound = 0;

    //Constructor
    public ExcelUtil() throws IOException {
        //Object member
        excelFile = new FileInputStream(new File(FILE_NAME));
        workbook = new XSSFWorkbook(excelFile);
        sheet = workbook.getSheet("participation short");
        calcColumnBound();
        calcRowBound();
    }

    /**
     * *************************************************Get and Setters***********************************
     */
    public void setColumnBound(int columnBound) {
        this.columnBound = columnBound;
    }

    public void setRowBound(int rowBound) {
        this.rowBound = rowBound;
    }

    public static int getColumnBound() {
        return columnBound;
    }

    public static int getRowBound() {
        return rowBound;
    }

    /**
     * ******************************************************Class methods*********************************
     */

    public void createNewSortedFile(List<List> personCollection){

        try {
            FILE_NAME = "C:\\Users\\Brio\\Desktop\\output.xlsx";
            workbook = new XSSFWorkbook();
            sheet = workbook.createSheet("NewSheet");

            CellStyle style = workbook.createCellStyle(); //Create new style
            style.setWrapText(false);

            int row = 1;
            for(List<Person> personList : personCollection){//person list has rows of information in it

                for(Person person : personList) {
                    //System.out.println(person.getCellList().get(0) + " " + person.matchesCount);
                    XSSFRow currentCell =  sheet.createRow(row);
                    List<String> cellList = person.getCellList();
                    for(int i = 0; i < cellList.size(); i++) {
                        currentCell.createCell(i).setCellValue(cellList.get(i));
                        sheet.setColumnWidth(i, 7000);
                        sheet.getRow(row).getCell(i).setCellStyle(style);
                    }
                    //System.out.println();
                    row++;
                }
            }

            FileOutputStream fileOut = new FileOutputStream(FILE_NAME);

            workbook.write(fileOut);
            workbook.close();

            System.out.print("Excel file created.");
        } catch(Exception e){
            System.out.println(e);
        }
    }

    public static String getRowCellData(int rowIndex, int colIndex){
        if(sheet.getRow(rowIndex).getCell(colIndex) != null)
            return sheet.getRow(rowIndex).getCell(colIndex).toString();

        return null;
    }

    private void calcColumnBound(){
        int i = 0;

        while(sheet.getRow(0).getCell(i, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL) != null){
            i++;
        }

        columnBound = i;
    }

    private void calcRowBound(){
        int i = 0;

        while(sheet.getRow(i).getCell(0, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL) != null){
            i++;
        }

        rowBound = i;
    }
}
