package structure;
import analysis.Person;

import java.util.ArrayList;
import java.util.List;

public class PersonNode {

    public PersonNode left = null;
    public PersonNode right = null;

    public List<Person> personGroup = null;

    PersonNode(){
        personGroup = new ArrayList<Person>();
    }

    //methods

    public void addToList(Person person){
        //System.out.println("Adding person to list");
        personGroup.add(person);
    }

}
