package structure;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;
import java.util.List;
import analysis.Person;

public class PersonTree {

    PersonNode root =  null;
    List<List> groupCollection = new ArrayList<List>();

    public PersonTree(List<Person> personList){

        createKeywordTree(personList);
        collectGroupLists(root);
        Collections.sort(groupCollection, new SortByMatches());

    }

    //getters
    public List getGroupCollection(){
        return groupCollection;
    }


    private void createKeywordTree(List<Person> personList){
        for(Person person : personList){
            //printArray(person);
            if(root == null){
                root = new PersonNode();
            }

            PersonNode currentNode = root;

            for(int i = 0; i < person.getCategoryArray().length; i++){
                if(person.getCategoryArray()[i] == 1){
                    if(currentNode.left != null) {
                        //System.out.println("LEFT");
                        currentNode = currentNode.left;
                    }
                    else {
                        //System.out.println("NEW LEFT");
                        currentNode.left = new PersonNode();
                        currentNode = currentNode.left;
                    }
                } else{
                    if(currentNode.right != null) {
                        //System.out.println("RIGHT");
                        currentNode = currentNode.right;
                    }
                    else {
                        //System.out.println("NEW RIGHT");
                        currentNode.right = new PersonNode();
                        currentNode = currentNode.right;
                    }
                }
            }

            currentNode.addToList(person);

        }
    }

    private void collectGroupLists(PersonNode node){

        if(node.left != null)
            collectGroupLists(node.left);

        if(node.right != null)
            collectGroupLists(node.right);

        if(node.right == null && node.left == null) {
            groupCollection.add(node.personGroup);
            return;
        }
    }

    //debug
    private void printArray(Person person){
        for(int i = 0; i < person.getCategoryArray().length; i++){
            System.out.print(person.getCategoryArray()[i] + " ");
        }
        System.out.println();
    }

    class SortByMatches<T extends Comparable<Person>> implements Comparator<List<Person>> {
        @Override
        public int compare(List<Person> p1, List<Person> p2){

            if(p1.get(0).getMatchesCount() >= p2.get(0).getMatchesCount())
                return -1;
            else if(p1.get(0).getMatchesCount() < p2.get(0).getMatchesCount())
                return 1;
            else
                return 0;

        }
    }


}
